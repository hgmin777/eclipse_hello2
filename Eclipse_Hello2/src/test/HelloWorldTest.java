package test;

import static org.junit.Assert.*;
import hello.HelloWorld;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HelloWorldTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testSay() {
		// fail("Not yet implemented"); // TODO
		HelloWorld hw = new HelloWorld();
		
		assertNotNull(hw);
		assertEquals(hw.say("안녕하세요?"), "안녕하세요?");		
	}
}
